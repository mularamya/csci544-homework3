Error Correction
=================

1) Initially installed NLTK and downloaded Brown and Reuters Corpus datasets.

2) Also downloaded pre-processed data from wikipedia ad the above data was not enough for training.

3) Extracted only the required lines from Brown Corpus Dataset using the script "first.py".

4) Extracted only the required lines from Wikipedia Dump using the script "second.py"

5) Built a multitron perceptron from scratch which used the vector and the average vector for training the data.

    - error_correction.py is the file used to do the training.
    - shuffled the lines of the training data for better learning
    - ran the perceptron for about 25 - 30 iterations and generated a model file
    
6) format_test.py was used on the test data to format the test file.

7) Once done with the formatting, the model file along with the formatted test file was used to classify the data.

8) classify.py is the classifier. It dumps the corrected words into an array called result.

9) Finally, final_writing.py is used to modify the unformatted testfile with the corrected words in the result array.

[ Note: Scripts for formatting the files were coded using Python's second version as Python3 doesn't work well with NLTK. ]

Steps for Execution
====================

1) python first.py > out1  [ For formatting the brown data ]

2) python second.py reuters_dataset > out2

3) python second.py wikipedia_dump > out3

4) cat out1 out2 out3 > out

5) python3 error_correction.py out model [ For training the data ]

6) python format_test.py hw3.test > formatted_test

7) python3 classify.py model formatted_test

8) python3 final_writing.py hw3.test > final_test

9) final_test will have the final output file which is hw3.output.txt