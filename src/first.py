#codin=utf-8
import nltk

from nltk.corpus import brown 

collection = { "its", "it's", "Its", "It's", "your", "you're", "Your", "You're", "they're", "their", "They're", "Their", "loose" , "lose", "Loose", "Lose", "to", "too", "To", "Too" }

correction1 = { "its", "it's", "Its", "It's" }

correction2 = {"your", "you're", "Your", "You're" }

correction3 = { "they're", "their", "They're", "Their" }

correction4 = {"loose" , "lose", "Loose", "Lose"}

correction5 = {"to", "too", "To", "Too"}

for sentence in brown.tagged_sents():

	sentence.insert(0, ("BEGIN", "BEGIN"))
	sentence.append( ("END","END") )	

	#print sentence
	
	for (prev,t1), (curr,t2), (nxt,t3) in nltk.trigrams(sentence):
	
		 if curr in collection:

                        if curr in correction1:
                                feature = "correction1"

                        elif curr in correction2:
                                feature = "correction2"

                        elif curr in correction3:
                                feature = "correction3"

                        elif curr in correction4:
                                feature = "correction4"

                        else:
                                feature = "correction5"
                        print curr, prev, curr, nxt, feature

