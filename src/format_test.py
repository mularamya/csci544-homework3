import sys

import nltk

from nltk.tokenize import regexp_tokenize

fin = open(sys.argv[1], "read")

collection = { "its", "it's", "Its", "It's", "your", "you're", "Your", "You're", "they're", "their", "They're", "Their", "loose" , "lose", "Loose", "Lose", "to", "too", "To", "Too" }

correction1 = { "its", "it's", "Its", "It's" }

correction2 = {"your", "you're", "Your", "You're" }

correction3 = { "they're", "their", "They're", "Their" }

correction4 = {"loose" , "lose", "Loose", "Lose"}

correction5 = {"to", "too", "To", "Too"}

for line in fin:

	array_words =  regexp_tokenize(line, pattern=("[A-Za-z'-']+|[A-Za-z']+|[^\w\s]+"))

	array_words.insert(0, "BEGIN")
	array_words.append("END")

        #print sentence

	#print array_words
        
	for prev, curr, nxt in nltk.trigrams(array_words):
		
		#print curr
		if curr in collection:

			if curr in correction1:	
				feature = "correction1"

			elif curr in correction2:
				feature = "correction2"
			
			elif curr in correction3:
				feature = "correction3"
		
			elif curr in correction4:
				feature = "correction4"

			else:
				feature = "correction5"

			print prev, curr, nxt
                        #print 

