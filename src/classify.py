import sys
import pickle

result = []

average_vector = {}

average_vector = pickle.load(open(sys.argv[1],"rb"))

collection = { "its", "it's", "Its", "It's", "your", "you're", "Your", "You're", "they're", "their", "They're", "Their", "loose" , "lose", "Loose", "Lose", "to", "too", "To", "Too" }

classes  = { "its", "it's", "your", "you're", "they're", "their", "loose" , "lose", "to", "too" }

correction1 = { "its", "it's", "Its", "It's" }

correction2 = {"your", "you're", "Your", "You're" }

correction3 = { "they're", "their", "They're", "Their" }

correction4 = {"loose" , "lose", "Loose", "Lose"}

correction5 = {"to", "too", "To", "Too"}

fin = open(sys.argv[2],"r", errors = 'ignore')
correct = 0
feature = 0

for line in fin:

	features = []
	avg = {}

	words = str.split(line) 
	
	prev = "prev_"+words[0]	
	
	nxt = "next_"+words[2]
	actual_word = words[1]

	if actual_word in collection:

		if actual_word in correction1:
			cat1 = "its"
			cat2 = "it's"
			feature = "correction1"

		elif actual_word in correction2:
			cat1 = "your"
			cat2 = "you're"
			feature = "correction2"

		elif actual_word in correction3:
			cat1 = "they're"
			cat2 = "their"
			feature = "correction3"

		elif actual_word in correction4:
			cat1 = "loose"
			cat2 = "lose"
			feature = "correction4"

		else:
			cat1 = "to"
			cat2 = "too"
			feature = "correction5"

		features.append(prev)
		#features.append(curr)
		features.append(nxt)
		features.append(feature)

		for every_feature in features:
			if every_feature in average_vector[cat1]:
				#print ( every_feature)
				avg[cat1] = avg.get(cat1, 0) + average_vector[cat1][every_feature]

				avg[cat2] = avg.get(cat2, 0) + average_vector[cat2][every_feature]


		if avg[cat1] < avg[cat2]:
			correct = cat2
		else:
			correct = cat1

		case = 0
		if actual_word in correction1:

			if actual_word  == "Its" and  correct == "its":
				result.append("Its")
		
			elif actual_word  == "Its" and  correct == "it's":
				result.append("It's")

			elif actual_word  == "It's" and correct == "it's":
				result.append("It's")

			elif actual_word == "It's" and correct == "its":
				result.append("Its")
		
			else:
				case = 1

		if actual_word in correction2:
			
			if actual_word  == "Your" and  correct == "your":
				result.append("Your")
		
			elif actual_word  == "Your" and  correct == "You're":
				result.append("You're")

			elif actual_word  == "You're" and correct == "you're":
				result.append("You're")

			elif actual_word == "You're" and correct == "your":
				result.append("Your")

			else:
				case = 1

		if actual_word in correction3:
	
			if actual_word  == "Their" and  correct == "their":
				result.append("Their")
		
			elif actual_word  == "Their" and  correct == "they're":
				result.append("They're")

			elif actual_word  == "They're" and correct == "they're":
				result.append("They're")

			elif actual_word == "They're" and correct == "their":
				result.append("Their")
			
			else:
				case = 1


		if actual_word in correction4:
	
			if actual_word  == "Loose" and  correct == "loose":
				result.append("Loose")
		
			elif actual_word  == "Loose" and  correct == "lose":
				result.append("Lose")

			elif actual_word  == "Lose" and correct == "lose":
				result.append("Lose")

			elif actual_word == "Lose" and correct == "loose":
				result.append("Loose")

			else:
				case = 1

		if actual_word in correction5:
		
			if actual_word  == "Too" and  correct == "too":
				result.append("Too")
		
			elif actual_word  == "Too" and  correct == "to":
				result.append("To")

			elif actual_word  == "To" and correct == "to":
				result.append("To")

			elif actual_word == "To" and correct == "too":
				result.append("Too")
			
			else: 
				case = 1		
		if case == 1:
			result.append(correct)			


#print(len(result))

pickle.dump(result, open("result_model","wb"))
