import sys
import pickle
import re

result = []

result = pickle.load(open(sys.argv[1], "rb"))

fin = open(sys.argv[2], "r", errors= 'ignore')

put = []

correction1 = { "its", "it's", "Its", "It's" }

correction2 = {"your", "you're", "Your", "You're" }

correction3 = { "they're", "their", "They're", "Their" }

correction4 = {"loose" , "lose", "Loose", "Lose"}

correction5 = {"to", "too", "To", "Too"}

if ( len(result) !=0 ): 
	r = result.pop(0) 
else: 
	r = "XXX"

for line in fin:
	words = str.split(line)
	wo = words
	for count,word in enumerate(words):

		w = re.search(r"\b(its|it's|Its|It's)\b",word,0)

		if r in correction1 and w:

			wo[count] = re.sub(r"\b(its|it's|Its|It's)\b",r,word)

			if ( len(result) !=0 ): 
				r = result.pop(0) 
			else: r = "XXX"


		w = re.search(r"\b(your|you're|Your|You're)\b",word,0)

		if r in correction2 and w:

			wo[count] = re.sub(r"\b(your|you're|Your|You're)\b",r,word)

			if ( len(result) !=0 ): r = result.pop(0) 
			else: r = "XXX"


		w = re.search(r"\b(they're|They're|their|Their)\b",word,0)

		if r in correction3 and w:

			wo[count] = re.sub(r"\b(they're|They're|their|Their)\b",r,word)

			if ( len(result) !=0 ): r = result.pop(0) 
			else: r = "XXX"


		w = re.search(r"\b(loose|Loose|lose|Lose)\b",word,0)

		if r in correction4 and w:
			wo[count] = re.sub(r"\b(loose|Loose|lose|Lose)\b",r,word)
			if ( len(result) !=0 ): r = result.pop(0) 
			else: r = "XXX"


		w = re.search(r"\b(too|Too|to|To)\b",word,0)

		if r in correction5 and w:
			wo[count] = re.sub(r"\b(too|Too|to|To)\b",r,word)
			if ( len(result) !=0 ): r = result.pop(0) 
			else: r = "XXX"

	if len(wo) == 0:
		print()
	else:
		print (' '.join(wo)+" ")
		


