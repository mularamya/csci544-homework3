import sys
import copy
import pickle
import random
import re
import codecs

fin = open(sys.argv[1], "r", encoding="ascii", errors="backslashreplace")

classes  = { "its", "it's", "Its", "It's", "your", "you're", "Your", "You're", "they're", "their", "Their", "They're", "loose" , "lose", "Loose", "Lose", "to", "too" ,"To", "Too"}

correction1 = { "its", "it's", "Its", "It's" }

correction2 = {"your", "you're", "Your", "You're" }

correction3 = { "they're", "their", "They're", "Their" }

correction4 = {"loose" , "lose", "Loose", "Lose"}

correction5 = {"to", "too", "To", "Too"}

vocab = {}
vector = {}
average_vector = {}
lines = []

for count,line in enumerate(fin):
	words =  str.split(line)
	correct = words[0]

	prev = "prev_"+words[1]
	nxt = "next_"+words[3]
	#curr = "curr_"+words[2].lower()
	feature = words[4]
	actual_word = words[2]

	vocab[prev] = 0
	vocab[nxt] = 0
	#vocab[curr] = 0
	#vocab[actual_word] = 0
	vocab[feature] = 0	

	line_to_append = correct + " " + prev + " " + nxt + " " + feature + " " + actual_word
	lines.append(line_to_append)

fin.close()


for every_class in classes:
		
	vector[every_class] = copy.deepcopy(vocab)
	average_vector[every_class] = copy.deepcopy(vocab)


i = 0
n = len(lines) 	
avg = {}
	
cat1 = 0
cat2 = 0 


for iterate in range(1,25):
	
	random.shuffle(lines)
	i = 0
	avg = {}
	
	for category in classes:
		for every_word in vocab:
			curr_value = vector[category][every_word]
			average_vector[category][every_word] = average_vector[category][every_word] + ( n * curr_value )
	for line in lines:
		#print (line)
		i = i + 1
		
		words = str.split(line);
	
		correct = words[0]
		#curr = words[2]
		group = words[3]
		actual_word = words[4]
		
		words.pop(0)
	
		if actual_word in correction1:
			cat1 = "its"
			cat2 = "it's"

		elif actual_word in correction2:
			cat1 = "your"
			cat2 = "you're"
	
		elif actual_word in correction3:
			cat1 = "they're" 
			cat2 = "their"

		elif actual_word in correction4:
			cat1 = "loose"
			cat2 = "lose"
	
		else:
			cat1 = "to"
			cat2 = "too"
	
		#print ("actual_word=", actual_word)
		for cat in [ cat1, cat2 ]:
			for every_word in words:
				if every_word != actual_word:
					avg[cat] = avg.get(cat, 0) + vector[cat][every_word]
		
		classify = sorted(avg, key=avg.get,reverse = True)[0]	
	
		lower = actual_word.lower()
		classify = classify.lower()

		#print ("corr = ", lower)
		#print ("classify = ", classify)	
		if classify != lower:
			 
			for every_word in words:
	
				if every_word != actual_word:
					previous = vector[classify][every_word]
					vector[classify][every_word] = vector[classify].get(every_word) - 1
					current = vector[classify][every_word]
					average_vector[classify][every_word] = average_vector[classify][every_word]-((n-i+1)*previous) + ((n-i+1)*current)

					previous = vector[lower][every_word]
					vector[lower][every_word] = vector[lower].get(every_word) + 1
					current = vector[lower][every_word]
					temp = average_vector[lower][every_word] - ((n-i+1) * previous) + ((n-i+1) * current)
					average_vector[lower][every_word] = temp
		
		avg = {}

#print ( average_vector )
pickle.dump(average_vector, open(sys.argv[2], "wb"))	
